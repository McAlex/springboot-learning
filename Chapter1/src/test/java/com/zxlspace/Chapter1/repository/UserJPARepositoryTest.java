package com.zxlspace.Chapter1.repository;

import com.zxlspace.Chapter1.pojo.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserJPARepositoryTest {

    @Autowired
    private UserJpaRepository userJPARepository;

    @Before
    public void before() {
        userJPARepository.save(new User("啊啊啊", 25));
    }

    @Test
    public void findByName() {

        User u1 = userJPARepository.findByName("啊啊啊");
        System.out.println("第一次查询 : " + u1.getAge());
        System.out.println("第一次查询 : " + u1.toString());

        User u2 = userJPARepository.findByName("啊啊啊");
        System.out.println("第二次查询 : " + u2.getAge());
        System.out.println("第二次查询 : " + u2.toString());
    }
}