package com.zxlspace.Chapter1;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: rdis测试
 * @Package com.zxlspace.Chapter1
 * @Description:
 * @date 2018/6/14 9:42
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationRedisTest {
    //StringRedisTemplate对象进行Redis的读写操作，该对象从命名中就可注意到支持的是String类型。
    // 如果有使用过spring-data-redis的开发者一定熟悉RedisTemplate<K, V>接口，StringRedisTemplate就相当于RedisTemplate<String, String>的实现。
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void testRedis() {
        redisTemplate.opsForValue().set("test", "100",60*10, TimeUnit.SECONDS);//向redis里存入数据和设置缓存时间
        Assert.assertEquals("100", redisTemplate.opsForValue().get("test"));
    }
}
