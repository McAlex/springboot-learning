package com.zxlspace.Chapter1.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GirlControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getGirls() throws Exception {
        //mockmvc 模拟http请求
        mockMvc.perform(MockMvcRequestBuilders.get("/girls")) //路由
                .andExpect(MockMvcResultMatchers.status().isOk()) //返回结果状态比较
                .andExpect(MockMvcResultMatchers.content().string("abc")); //返回结果比较
    }
}