package com.zxlspace.Chapter1.task;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Future;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AsyncTaskTest {
    @Autowired
    private AsyncTask task;

    @Test
    public void doTaskOne() throws Exception {
        long start = System.currentTimeMillis();

        Future<String> task1 = task.doTaskOne();
        Future<String> task2 = task.doTaskTwo();
        Future<String> task3 = task.doTaskThree();

        while (true) {
            if (task1.isDone() && task2.isDone() && task3.isDone()) {
                //三个任务都完成，跳出循环等待
                break;
            }
            Thread.sleep(1000);
        }

        long end = System.currentTimeMillis();
        log.info("任务全部完成，总耗时" + (end - start) + "毫秒");
    }
}