package com.zxlspace.Chapter1;

import com.zxlspace.Chapter1.properties.BlogProperties;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 测试案例
 * @Package com.zxlspace.Chapter1
 * @Description:
 * @date 2018/6/5 17:10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Chapter1ApplicationTestDemo {

    @Autowired
    private BlogProperties blogProperties;

    @Value("${test.temp.name}")
    private String tempName;

    @Test
    public void testProperties() throws Exception{
        System.out.println(blogProperties.getName());
        System.out.println(blogProperties.getDesc());
        Assert.assertEquals(blogProperties.getName(), "程序猿DD");
        Assert.assertEquals(blogProperties.getName(), "Spring Boot教程");

        System.out.println("随机字符串：" + blogProperties.getStrValue());
        System.out.println("随机int：" + blogProperties.getIntValue());

        System.out.println("测试属性配置:" + tempName);
    }
}
