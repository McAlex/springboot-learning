package com.zxlspace.Chapter1.mapper;

import com.zxlspace.Chapter1.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class UserMapperTest {

    @Autowired
    UserMapper userMapper;

    @Test
    public void findByName() {
        User user = userMapper.findByName("东北老");
        System.out.println(user);
        Assert.assertEquals(10, user.getAge().intValue());
    }

    @Test
    public void insert() {
       int result = userMapper.insert("东北老", 10);
        Assert.assertEquals(1, result);
    }

    @Test
    public void testInsertByMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "abc");
        map.put("age", 22);
        int result = userMapper.insertByMap(map);
        Assert.assertEquals(1, result);
    }

    @Test
    public void testInsertByUser() {
        User user = new User("哈哈哈", 88);
        int result = userMapper.insertByUser(user);
        System.out.println("新增user对象结果：" + result);
        Assert.assertEquals(1, result);
    }

    @Test
    public void update() {
        User user = new User("哈哈哈", 99);
        int result = userMapper.update(user);
        System.out.println("更新user对象结果：" + result);
        Assert.assertEquals(1, result);
    }

    @Test
    public void delete() {
         userMapper.deleteUser(6L);
    }

    @Test
    public void findAll() {
        List<User> userList = userMapper.findAll();
        System.out.println(userList);
        for (User user : userList) {
            Assert.assertEquals(null, user.getId());
            Assert.assertNotEquals(null, user.getName());
        }
    }
}