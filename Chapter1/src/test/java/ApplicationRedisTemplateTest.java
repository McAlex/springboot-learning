import com.zxlspace.Chapter1.pojo.RedisUser;
import javafx.application.Application;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package PACKAGE_NAME
 * @Description:
 * @date 2018/6/14 10:30
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationRedisTemplateTest {
    @Resource
    private RedisTemplate<String, RedisUser> redisTemplate;

    @Test
    public void test() throws Exception {
        //保存对象
        RedisUser user = new RedisUser("超人", 20);
        redisTemplate.opsForValue().set(user.getUserName(), user);

        user = new RedisUser("蝙蝠侠", 30);
        redisTemplate.opsForValue().set(user.getUserName(), user);

        user = new RedisUser("蜘蛛侠", 40);
        redisTemplate.opsForValue().set(user.getUserName(), user);

        Assert.assertEquals(20, redisTemplate.opsForValue().get("超人").getAge().longValue());
        Assert.assertEquals(30, redisTemplate.opsForValue().get("蝙蝠侠").getAge().longValue());
        Assert.assertEquals(40, redisTemplate.opsForValue().get("蜘蛛侠").getAge().longValue());
    }
}
