package com.zxlspace.Chapter1.service;

import com.zxlspace.Chapter1.enums.ResultEnum;
import com.zxlspace.Chapter1.exception.GirlException;
import com.zxlspace.Chapter1.pojo.Girl;
import com.zxlspace.Chapter1.repository.GirlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package com.zxlspace.Chapter1.service
 * @Description:
 * @date 2018/6/12 17:56
 */
@Service
public class GirlService {
    @Autowired
    private GirlRepository girlRepository;

    public void getAge(Integer id) throws Exception {
        Girl girl = girlRepository.findOne(id);
        Integer age = girl.getAge();
        if (age <= 10) {
           // throw new Exception("你可能还在小学！");
//            throw new GirlException(100, "你可能还在小学！");
            throw new GirlException(ResultEnum.PRIMARY_SCHOOL);
        }else if (age > 10 && age < 16) {
            //throw new Exception("你看你还在初中！");
//            throw new GirlException(101, "你看你还在初中！");
            throw new GirlException(ResultEnum.MIDDLE_SCHOOL);
        }else {
            //throw new Exception("你可能已经上大学了！");
//            throw new GirlException(102, "你可能已经上大学了！");
            throw new GirlException(ResultEnum.HIGN_SCHOOL);
        }
    }
}
