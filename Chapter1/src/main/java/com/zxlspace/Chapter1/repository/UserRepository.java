package com.zxlspace.Chapter1.repository;

import com.zxlspace.Chapter1.pojo.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: mongodb jpa
 * @Package com.zxlspace.Chapter1.repository
 * @Description:
 * @date 2018/7/5 18:00
 */
public interface UserRepository extends MongoRepository<User, Long>{

    User findByName(String userName);
}
