package com.zxlspace.Chapter1.repository;

import com.zxlspace.Chapter1.pojo.User;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: mongodb jpa
 * @Package com.zxlspace.Chapter1.repository
 * @Description:
 * @date 2018/7/5 18:00
 */
@CacheConfig(cacheNames = "users")
public interface UserJpaRepository extends JpaRepository<User, Long> {

    @Cacheable
    User findByName(String userName);
}
