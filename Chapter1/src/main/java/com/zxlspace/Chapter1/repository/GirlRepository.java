package com.zxlspace.Chapter1.repository;

import com.zxlspace.Chapter1.pojo.Girl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: jpa 持久层
 * @Package com.zxlspace.Chapter1.repository
 * @Description:
 * @date 2018/6/8 14:18
 */
public interface GirlRepository extends JpaRepository<Girl, Integer> {
    /**
     * 通过年龄查询
     * @param age
     * @return
     */
    List<Girl> findByAge(Integer age);
}
