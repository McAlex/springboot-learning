package com.zxlspace.Chapter1.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: redis序列化对象
 * @Package com.zxlspace.Chapter1.pojo
 * @Description:
 * @date 2018/6/14 10:17
 */
@Data
public class RedisUser implements Serializable{
    private String  userName;
    private Integer age;

    public RedisUser(String userName, Integer age) {
        this.userName = userName;
        this.age = age;
    }
}
