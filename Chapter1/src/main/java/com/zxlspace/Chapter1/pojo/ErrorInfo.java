package com.zxlspace.Chapter1.pojo;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 错误信息处理pojo
 * @Package com.zxlspace.Chapter1.pojo
 * @Description:
 * @date 2018/6/8 9:57
 */
@lombok.Data
public class ErrorInfo<T> {
    public static final Integer OK = 0;
    public static final Integer ERROR = 100;

    private Integer code;

    private String message;

    private String url;

    private T Data;
}
