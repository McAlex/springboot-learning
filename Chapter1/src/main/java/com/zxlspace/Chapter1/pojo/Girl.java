package com.zxlspace.Chapter1.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package com.zxlspace.Chapter1.pojo
 * @Description:
 * @date 2018/6/8 11:56
 */
@Entity
@NoArgsConstructor
@Data
public class Girl {

    @Id
    @GeneratedValue
    private Integer id;

    private String cupSize;

//    @Min(value = 18, message = "未成年少女禁止入内")
    private Integer age;

    @NotNull(message = "金额必传")
    private Integer money;

}
