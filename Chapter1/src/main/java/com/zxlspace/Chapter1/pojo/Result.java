package com.zxlspace.Chapter1.pojo;

import lombok.Data;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 返回结果集对象
 * @Package com.zxlspace.Chapter1.pojo
 * @Description:
 * @date 2018/6/12 17:44
 */
@Data
public class Result<T> {
    /**错误码**/
    private Integer code;
    /**错误信息**/
    private String message;
    /**结果集**/
    private T data;
}
