package com.zxlspace.Chapter1.controller;

import com.zxlspace.Chapter1.exception.MyException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: spring boot rest服务
 * @Package com.zxlspace.Chapter1.controller
 * @Description:
 * @date 2018/6/5 17:05
 */
@RestController
public class HelloController {

    /**
     * Spring4之后加入的注解，原来在@Controller中返回json需要@ResponseBody来配合，如果直接用@RestController替代@Controller就不需要再配置@ResponseBody，默认返回json格式
     * @return
     */
//    @RequestMapping(value = {"/hello", "hi"}, method = RequestMethod.GET)
    @GetMapping(value = {"/hello", "hi"})
    public String index() throws Exception {
//        return "Hello world!";
        throw new Exception("发生错误1");
    }

    @RequestMapping("/json")
    public String json() throws Exception{
        throw new MyException("发送错误2");
    }


}
