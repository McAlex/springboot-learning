package com.zxlspace.Chapter1.controller;

import com.zxlspace.Chapter1.properties.GirlProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 测试Thymeleaf的模板
 * @Package com.zxlspace.Chapter1.controller
 * @Description:
 * @date 2018/6/7 15:16
 */
@Controller
@RequestMapping(value = "/page")
public class PageController {
    @Autowired
    private GirlProperties girlProperties;

    @RequestMapping("/index")
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("host", "http://blog.didispace.com");
        // return模板文件的名称，对应src/main/resources/templates/index.html
        modelMap.addAttribute("girl", girlProperties);
        return "/index";
    }
}
