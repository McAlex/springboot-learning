package com.zxlspace.Chapter1.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 定时任务
 * @Package com.zxlspace.Chapter1.schedule
 * @Description:
 * @date 2018/7/12 16:25
 */
@Component
public class ScheduledTasks {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:ss:mm");

    /**
     * 使用了@Scheduled(fixedRate = 5000) 注解来定义每过5秒执行的任务
     * @Scheduled(fixedRate = 5000) ：上一次开始执行时间点之后5秒再执行;
     * @Scheduled(fixedDelay = 5000) ：上一次执行完毕时间点之后5秒再执行;
     * @Scheduled(initialDelay=1000, fixedRate=5000) ：第一次延迟1秒后执行，之后按fixedRate的规则每5秒执行一次;
     */
    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        //@Scheduled(cron="*/5 * * * * *") ：通过cron表达式定义规则
        System.out.println("现在时间:" + sdf.format(new Date()));
    }
}
