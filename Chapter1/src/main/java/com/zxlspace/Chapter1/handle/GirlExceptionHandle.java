package com.zxlspace.Chapter1.handle;

import com.zxlspace.Chapter1.enums.ResultEnum;
import com.zxlspace.Chapter1.exception.GirlException;
import com.zxlspace.Chapter1.pojo.Result;
import com.zxlspace.Chapter1.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 异常统一捕获器
 * @Package com.zxlspace.Chapter1.handle
 * @Description:
 * @date 2018/6/13 9:43
 */
@ControllerAdvice
public class GirlExceptionHandle {
    Logger logger = LoggerFactory.getLogger(GirlExceptionHandle.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handle(Exception e) {
        logger.error("捕获异常>>>>>>>>", e.getMessage());
        if (e instanceof GirlException) { //判断当前异常是否是girlException
            GirlException girlException = (GirlException) e;
            return ResultUtil.error(girlException.getCode(), girlException.getMessage());
        }else {
            logger.error("【系统异常】{}", e);
            return ResultUtil.error(ResultEnum.UNKOWN_ERROR.getCode(), e.getMessage());
        }
    }

    @ExceptionHandler(value = GirlException.class)
    @ResponseBody
    public Result girlExceptionHandle(GirlException e) {
        logger.error("捕获自定义异常girlException》》》》》》", e.getMessage());
        return ResultUtil.error(e.getCode(), e.getMessage());
    }
}
