package com.zxlspace.Chapter1.utils;

import com.zxlspace.Chapter1.enums.ResultEnum;
import com.zxlspace.Chapter1.pojo.Result;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 结果工具类
 * @Package com.zxlspace.Chapter1.utils
 * @Description:
 * @date 2018/6/12 17:47
 */
public class ResultUtil {
    public static Result success(Object object) {
        Result result = new Result();
        result.setCode(ResultEnum.SUCCESS.getCode());
        result.setMessage(ResultEnum.SUCCESS.getMsg());
        result.setData(object);
        return result;
    }

    public static Result success() {
        return success(null);
    }

    public static Result error(Integer code, String message) {
        Result result = new Result();
        result.setCode(code);
        result.setMessage(message);
        return result;
    }
}
