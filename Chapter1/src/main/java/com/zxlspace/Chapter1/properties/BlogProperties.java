package com.zxlspace.Chapter1.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package com.zxlspace.Chapter1.pojo
 * @Description:
 * @date 2018/6/5 17:27
 */
//@component （把普通pojo实例化到spring容器中，相当于配置文件中的<bean id="" class=""/>）
@Component
@Data
public class BlogProperties {
    @Value("${com.didispace.blog.name}")
    private String name;
    @Value("${com.didispace.blog.title}")
    private String title;
    @Value("${com.didispace.blog.desc}")
    private String desc;

    @Value("${com.didispace.blog.value}")
    private String strValue;

    @Value("${com.didispace.blog.int}")
    private String intValue;
}
