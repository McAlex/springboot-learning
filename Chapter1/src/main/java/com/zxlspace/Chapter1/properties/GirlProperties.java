package com.zxlspace.Chapter1.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author syncwt
 * @version V1.0
 * @Title: 配置自动注入
 * @Package zhangxiaolin
 * @Description: TODO
 * @date
 */
@Component
@ConfigurationProperties(prefix = "girl")
@Data
public class GirlProperties {
    private String name;

    private String cupSize;

    private int age;

}
