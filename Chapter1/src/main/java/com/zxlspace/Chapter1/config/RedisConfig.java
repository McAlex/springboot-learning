/*
package com.zxlspace.Chapter1.config;

import com.zxlspace.Chapter1.pojo.RedisUser;
import com.zxlspace.Chapter1.redis.RedisObjectSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

*/
/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 配置针对User的RedisTemplate实例
 * @Package com.zxlspace.Chapter1.config
 * @Description:
 * @date 2018/6/14 10:25
 *//*

@Configuration
public class RedisConfig {

    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        return new JedisConnectionFactory();
    }

    @Bean
    public RedisTemplate<String, RedisUser>  redisTemplate() {
        RedisTemplate<String, RedisUser> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new RedisObjectSerializer());
        return template;
    }
}
*/
