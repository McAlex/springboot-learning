package com.zxlspace.Chapter1.enums;

public enum ResultEnum {
    UNKOWN_ERROR(-1, "未知错误"),
    SUCCESS(0, "成功处理"),
    PRIMARY_SCHOOL(100, "你可能是小学生"),
    MIDDLE_SCHOOL(101, "你可能是初中生"),
    HIGN_SCHOOL(102, "你可能是高中生")
    ;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private Integer code;

    private String  msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
