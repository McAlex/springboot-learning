package com.zxlspace.Chapter1.mapper;

import com.zxlspace.Chapter1.pojo.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package com.zxlspace.Chapter1.mapper
 * @Description:
 * @date 2018/7/5 16:26
 */
@Mapper
@Repository
public interface UserMapper {

    @Select("SELECT * FROM user where name = #{name}")
    User findByName(@Param("name") String name);

    @Update("UPDATE `user` SET `age` = #{age} WHERE `name` = #{name}")
    int update(User user);

    @Delete("DELETE FROM `user` where id = #{id}")
    int deleteUser(Long id);

    //@Result中的property属性对应User对象中的成员名，column对应SELECT出的字段名
    @Results({
            @Result(property = "name", column = "name"),
            @Result(property = "age", column = "age")
    })
    @Select("Select name, age from user")
    List<User> findAll();

    /**
     * 使用@Param传参
     * @param name
     * @param age
     * @return
     */
    @Insert("INSERT INTO USER(NAME, AGE) VALUES (#{name}, #{age})")
    int insert(@Param("name") String name, @Param("age") Integer age);

    /**
     * 使用map传参
     * @param map
     * @return
     */
    @Insert("INSERT INTO USER(NAME, AGE) VALUES (#{name, jdbcType=VARCHAR}, #{age, jdbcType=INTEGER})")
    int insertByMap(Map<String, Object> map);

    /**
     * 使用user对象传参
     * @param user
     * @return
     */
    @Insert("INSERT INTO USER(NAME, AGE) VALUES (#{name}, #{age})")
    int insertByUser(User user);
}
