package com.zxlspace.Chapter1.designMode.Log;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package com.zxlspace.Chapter1.designMode.Log.ratify
 * @Description:
 * @date 2018/7/2 17:08
 */
public abstract class AbstractLogger {

    public static int INFO = 1;
    public static int DEBUG = 2;
    public static int ERROR = 3;

    protected int level;

    //责任链中的下一个元素
    protected AbstractLogger nextLogger;

    public void setNextLogger(AbstractLogger nextLogger){
        this.nextLogger = nextLogger;
    }

    public void logMessage(int level, String message){
        if(this.level <= level){
            write(message);
        }
        if(nextLogger !=null){
            nextLogger.logMessage(level, message);
        }
    }

    abstract protected void write(String message);

    public void nextLogger(AbstractLogger consoleLogger) {
    }
}
