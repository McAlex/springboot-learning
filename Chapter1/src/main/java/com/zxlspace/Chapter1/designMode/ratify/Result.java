package com.zxlspace.Chapter1.designMode.ratify;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 结果对象
 * @Package com.zxlspace.Chapter1.designMode.ratify
 * @Description:
 * @date 2018/7/2 17:58
 */
public class Result {

    public boolean isRatify;

    public String info;

    public Result() {
    }

    public Result(boolean isRatify, String info) {
        this.isRatify = isRatify;
        this.info = info;
    }


}
