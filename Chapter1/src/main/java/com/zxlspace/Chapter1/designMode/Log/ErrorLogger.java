package com.zxlspace.Chapter1.designMode.Log;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package com.zxlspace.Chapter1.designMode.Log
 * @Description:
 * @date 2018/7/2 17:15
 */
public class ErrorLogger extends AbstractLogger {

    public ErrorLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Error Console Logger:" + message);
    }
}
