package com.zxlspace.Chapter1.designMode.Log;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package com.zxlspace.Chapter1.designMode.Log
 * @Description:
 * @date 2018/7/2 17:14
 */
public class ConsoleLogger extends AbstractLogger {
    public ConsoleLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Standard Console::Logger : " + message);
    }
}
