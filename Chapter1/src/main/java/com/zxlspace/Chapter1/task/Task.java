package com.zxlspace.Chapter1.task;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 同步调用
 * @Package com.zxlspace.Chapter1.schedule
 * @Description:
 * @date 2018/7/12 16:39
 */
@Component
public class Task {

    public static Random random = new Random();

    public void doTaskOne() throws Exception {
        System.out.println("开始做任务一");
        long startTime = System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long endTime = System.currentTimeMillis();
        System.out.println("任务一完成，耗时:" + (endTime - startTime) + "毫秒");
    }

    public void doTaskTwo() throws Exception {
        System.out.println("开始做任务二");
        long startTime = System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long endTime = System.currentTimeMillis();
        System.out.println("任务二完成，耗时:" + (endTime - startTime) + "毫秒");
    }

    public void doTaskThree() throws Exception {
        System.out.println("开始做任务三");
        long startTime = System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long endTime = System.currentTimeMillis();
        System.out.println("任务三完成，耗时:" + (endTime - startTime) + "毫秒");
    }
}
