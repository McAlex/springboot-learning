package com.zxlspace.Chapter1.exception;

import com.zxlspace.Chapter1.enums.ResultEnum;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 自定义异常
 * @Package com.zxlspace.Chapter1.exception
 * @Description:
 * @date 2018/6/12 18:02
 */
public class GirlException extends RuntimeException{

    private Integer code;

  /*  public GirlException(Integer code, String message) {
        super(message);
        this.code = code;
    }*/

    public GirlException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
