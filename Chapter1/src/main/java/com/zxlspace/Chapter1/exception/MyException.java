package com.zxlspace.Chapter1.exception;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 自定义异常类
 * @Package com.zxlspace.Chapter1.exception
 * @Description:
 * @date 2018/6/8 10:03
 */
public class MyException extends RuntimeException {
    public MyException(String message) {
        super(message);
    }
}
