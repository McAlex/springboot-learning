package com.imooc.product.dataobject;

import lombok.Data;

import java.util.Date;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 基础实体
 * @Package com.zxlspace.weixinseller.dataobject
 * @Description:
 * @date 2018/6/14 11:51
 */
@Data
public class BaseDto {
    private Date createTime;

    private Date updateTime;
}
