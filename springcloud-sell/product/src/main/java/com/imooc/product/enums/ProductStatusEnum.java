package com.imooc.product.enums;

import lombok.Getter;

@Getter
public enum ProductStatusEnum {
    PRODUCT_UP(0, "商品上架"),
    PRODUCT_DOWN(1, "商品下架")
    ;
    private Integer code;
    private String message;

    ProductStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
