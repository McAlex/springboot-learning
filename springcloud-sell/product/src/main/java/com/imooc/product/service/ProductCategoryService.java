package com.imooc.product.service;


import com.imooc.product.dataobject.ProductCategory;

import java.util.List;

public interface ProductCategoryService {

//    ProductCategory findOne(Integer categoryId);

    List<ProductCategory> findAll();

    /**
     * 通过类型查询商品类目列表
     * @param categoryTypeList
     * @return
     */
    List<ProductCategory>  findByCategoryTypeIn(List<Integer> categoryTypeList);

    /**
     * 新增和更新
     * @param productCategory
     * @return
     */
    ProductCategory save(ProductCategory productCategory);
}
