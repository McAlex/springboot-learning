package com.imooc.product.repository;

import com.imooc.product.dataobject.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 类目资源库
 * @Package com.zxlspace.weixinseller.repository
 * @Description:
 * @date 2018/6/14 11:28
 */
public interface ProductCategotyRepository extends JpaRepository<ProductCategory, Integer> {

    /**
     * 通过类型查询商品类目列表
     * @param categoryTypeList
     * @return
     */
    List<ProductCategory>  findByCategoryTypeIn(List<Integer> categoryTypeList);
}
