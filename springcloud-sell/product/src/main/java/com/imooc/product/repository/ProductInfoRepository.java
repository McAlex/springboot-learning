package com.imooc.product.repository;

import com.imooc.product.dataobject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 商品信息资源库
 * @Package com.zxlspace.weixinseller.repository
 * @Description:
 * @date 2018/6/14 15:17
 */
public interface ProductInfoRepository extends JpaRepository<ProductInfo, String> {

    /**
     * 通过商品状态查询商品
     * @param productStatus
     * @return
     */
    List<ProductInfo> findByProductStatus(Integer productStatus);

    /**
     * 通过商品类目查询商品
     * @param categoryType
     * @return
     */
    List<ProductInfo> findByCategoryType(Integer categoryType);
}
