package com.imooc.product.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 商品列表（包含类目）
 * @Package com.zxlspace.weixinseller.VO
 * @Description:
 * @date 2018/6/14 17:32
 */
@Data
public class ProductVO {
    @JsonProperty("name")  //转换为json的时候自动把字段名称变为name
    private String categoryName;

    @JsonProperty("type")
    private Integer categoryType;

    @JsonProperty("foods")
    private List<ProductInfoVO> productInfoVOList;
}
