package com.imooc.product.service;

import com.imooc.product.dataobject.ProductInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 商品业务接口
 * @Package com.zxlspace.weixinseller.service
 * @Description:
 * @date 2018/6/14 16:13
 */
public interface ProductInfoService {
//    ProductInfo findOne(String productId);

    /**
     * 查询所有上架商品
     * @return
     */
    List<ProductInfo> findUpAll();

    /**
     * 分页查询商品
     * @param pageable
     * @return
     */
    Page<ProductInfo> findAll(Pageable pageable);

    ProductInfo save(ProductInfo productInfo);

    //加库存
//    void increaseStock(List<CartDTO> cartDTOList);

    //减库存
//    void decreaseStock(List<CartDTO> cartDTOList);
}
