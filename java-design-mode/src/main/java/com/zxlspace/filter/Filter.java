package com.zxlspace.filter;

/**
 * 责任链过滤接口
 * @author zhangxiaolin
 * @version V1.0
 * @Title:
 * @Package
 * @Description: TODO
 * @date
 */
public interface Filter {

    String doFilter(String str);
}
