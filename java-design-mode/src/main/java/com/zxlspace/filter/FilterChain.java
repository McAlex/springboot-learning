package com.zxlspace.filter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title: 过滤器链
 * @Package
 * @Description: TODO
 * @date
 */
public class FilterChain {

    List<Filter> filters = new ArrayList<Filter>();

    public void addFilter(Filter filter) {
        this.filters.add(filter);
    }

    public String doFilter(String str) {
        for (Filter filter : filters) {
           str = filter.doFilter(str);
        }
        return str;
    }
}
