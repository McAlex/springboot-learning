package com.zxlspace.filter;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title:
 * @Package
 * @Description: TODO
 * @date
 */
public class HtmlFilter implements Filter {

    public String doFilter(String str) {
        String r = str.replaceAll("<", "[");
        r = r.replaceAll(">", "]");
        return r;
    }
}
