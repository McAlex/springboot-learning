package com.zxlspace.filter;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title: 消息处理类
 * @Package
 * @Description: TODO
 * @date
 */
public class MsgProcessor {

    private String msg;

    //filter 过滤数组，形成过滤器链
//    Filter[] filters = {new HtmlFilter(), new SensitiveFilter()};
     FilterChain filterChain;

    public FilterChain getFilterChain() {
        return filterChain;
    }

    public void setFilterChain(FilterChain filterChain) {
        this.filterChain = filterChain;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String process() {
      /*  for (Filter f : filters) {
            r = f.doFilter(r);
        }*/

        return filterChain.doFilter(msg);
    }
}
