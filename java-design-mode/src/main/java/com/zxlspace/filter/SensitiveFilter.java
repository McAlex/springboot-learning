package com.zxlspace.filter;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title:
 * @Package
 * @Description: TODO
 * @date
 */
public class SensitiveFilter implements Filter {
    public String doFilter(String str) {
        return str.replace("被就业", "就业").replace("敏感", "");
    }
}
