package com.itmuch.cloud.microsofteurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class MicrosoftEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrosoftEurekaServerApplication.class, args);
	}
}
