package com.itmuch.cloud.controller;

import com.itmuch.cloud.entity.User;
import com.itmuch.cloud.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title:
 * @Package
 * @Description: TODO
 * @date
 */
@RequestMapping("/users")
@RestController
public class UserController {
    
    @Autowired
    private UserRepository userRepository;
    
    //@GetMapping，是Spring 4.3提供的新注解。它是一个组合注解，等价于@RequestMapping(method = RequestMethod.GET)，用于简化开发
    @GetMapping("/{id}")
    @ResponseBody
    public Optional<User> findById(@PathVariable Long id) {
        return this.userRepository.findById(id);
    }
}
