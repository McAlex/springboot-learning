package com.itmuch.cloud.repository;

import com.itmuch.cloud.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title:
 * @Package
 * @Description: TODO
 * @date
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
}
