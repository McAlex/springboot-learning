package com.itmuch.cloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title: 用户实体
 * @Package
 * @Description: TODO
 * @date
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column
    private String userName;
    
    @Column
    private String name;
    
    @Column
    private Integer age;
    
    @Column
    private BigDecimal balance;
}
