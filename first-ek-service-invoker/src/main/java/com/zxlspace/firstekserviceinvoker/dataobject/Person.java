package com.zxlspace.firstekserviceinvoker.dataobject;


import lombok.Setter;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title:
 * @Package
 * @Description: TODO
 * @date
 */
@Setter
public class Person {
    private String personId;

    private String name;

    private Integer age;

    public Person(String personId, String name, Integer age) {
        this.personId = personId;
        this.name = name;
        this.age = age;
    }
}
