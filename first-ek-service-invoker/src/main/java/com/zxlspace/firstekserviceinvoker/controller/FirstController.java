package com.zxlspace.firstekserviceinvoker.controller;

import com.zxlspace.firstekserviceinvoker.dataobject.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title:
 * @Package
 * @Description: TODO
 * @date
 */
@RestController
public class FirstController {

    @GetMapping(value = "/findPerson/{id}")
    public Person findPerson(@PathVariable("id") String personId) {
        Person person = new Person(personId, "crazyit", 30);
        return person;
    }
}
