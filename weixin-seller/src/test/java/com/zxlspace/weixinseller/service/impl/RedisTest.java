package com.zxlspace.weixinseller.service.impl;

import com.google.gson.Gson;
import com.zxlspace.weixinseller.dataobject.ProductInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * description: RedisTest <br>
 * date: 2020/4/13 15:16 <br>
 * author: zhangxiaolin <br>
 * version: 1.0 <br>
 */

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class RedisTest {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RedisTemplate<String, Object> selfRedisTemplate;

    @Autowired
    private ValueOperations<String, Object> redisValueTemplate;

    @Test
    public void testRedisTemplate() {
        ValueOperations<String, String> valueTemplate = redisTemplate.opsForValue();
        valueTemplate.set("StringKey1", "hello spring boot redis, String Redis");

        String value = valueTemplate.get("StringKey1");
        log.info("redis string value, {}", value);

        Gson gson = new Gson();
        ProductInfo productInfo = new ProductInfo().builder()
                .productId(UUID.randomUUID().toString().substring(0, 16)).productName("Iphone 8 PLUS")
                .productPrice(new BigDecimal("6666.99").setScale(BigDecimal.ROUND_CEILING))
                .productDescription("苹果智能手机")
                .categoryType(2).productIcon("http://static.roncoo.com/lecturer/ec8a20af4dcd42bfb88a25bc34ca5d97.jpg")
                .productStatus(1)
                .productStock(1000)
                .build();

        valueTemplate.set("StringKey2", gson.toJson(productInfo));
        ProductInfo redisProduct = gson.fromJson(valueTemplate.get("StringKey2"), ProductInfo.class);
        log.info("redisProduct={}", redisProduct);
    }

    @Test
    public void testRedisTemplate2() {
        ValueOperations<String, Object> valueTemplate = selfRedisTemplate.opsForValue();
        valueTemplate.set("StringKey1", "hello spring boot redis, String Redis");

        String value = (String) valueTemplate.get("StringKey1");
        log.info("redis string value, {}", value);

        ProductInfo productInfo = new ProductInfo().builder()
                .productId(UUID.randomUUID().toString().substring(0, 16)).productName("Iphone 8 PLUS")
                .productPrice(new BigDecimal("6666.99").setScale(BigDecimal.ROUND_CEILING))
                .productDescription("苹果智能手机")
                .categoryType(2).productIcon("http://static.roncoo.com/lecturer/ec8a20af4dcd42bfb88a25bc34ca5d97.jpg")
                .productStatus(1)
                .productStock(1000)
                .build();

        //会使用定义好的jackson 序列化类序列化为json
        valueTemplate.set("StringKey2", productInfo);
        ProductInfo redisProduct = (ProductInfo) valueTemplate.get("StringKey2");
        log.info("redisProduct={}", redisProduct);
    }

    @Test
    public void testRedisTemplate3() {
//        ValueOperations<String, Object> valueTemplate = selfRedisTemplate.opsForValue();
        redisValueTemplate.set("StringKey3", "hello spring boot redis, String Redis");

        String value = (String) redisValueTemplate.get("StringKey1");
        log.info("redis string value, {}", value);

        ProductInfo productInfo = new ProductInfo().builder()
                .productId(UUID.randomUUID().toString().substring(0, 16)).productName("Iphone 8 PLUS")
                .productPrice(new BigDecimal("6666.99").setScale(BigDecimal.ROUND_CEILING))
                .productDescription("苹果智能手机")
                .categoryType(2).productIcon("http://static.roncoo.com/lecturer/ec8a20af4dcd42bfb88a25bc34ca5d97.jpg")
                .productStatus(1)
                .productStock(1000)
                .build();

        //会使用定义好的jackson 序列化类序列化为json
        redisValueTemplate.set("StringKey4", productInfo);
        ProductInfo redisProduct = (ProductInfo) redisValueTemplate.get("StringKey2");
        log.info("redisProduct={}", redisProduct);
    }
}
