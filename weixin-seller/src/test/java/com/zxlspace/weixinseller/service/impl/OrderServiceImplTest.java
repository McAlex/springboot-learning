package com.zxlspace.weixinseller.service.impl;

import com.zxlspace.weixinseller.DTO.OrderDTO;
import com.zxlspace.weixinseller.dataobject.OrderDetail;
import com.zxlspace.weixinseller.enums.OrderStatusEnum;
import com.zxlspace.weixinseller.enums.PayStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderServiceImplTest {
    @Autowired
    private OrderServiceImpl orderService;

    private static final String BUYER_OPENID = "11011110";
    private static final String ORDER_ID = "1529549308311814147";

    @Test
    public void create() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName("马化腾");
        orderDTO.setBuyerPhone("15990010001");
        orderDTO.setBuyerAddress("深圳市福田区");
        orderDTO.setBuyerOpenId(BUYER_OPENID);

        List<OrderDetail> orderDetailList = new ArrayList<>();
        //购物车对象只传商品id和数量
        OrderDetail orderDetail = OrderDetail.builder()
                .productId("fd270b97-e1f8-4b")
                .productQuantity(2).build();

        OrderDetail orderDetail2 = OrderDetail.builder()
                .productId("fd270b97-e1f8-ass")
                .productQuantity(1).build();
        orderDetailList.add(orderDetail);
        orderDetailList.add(orderDetail2);

        orderDTO.setOrderDetailList(orderDetailList);

        OrderDTO od = orderService.create(orderDTO);
        log.info("新增订单响应, result={}", od);
    }

    @Test
    public void findOne() {
        OrderDTO orderDTO = orderService.findOne(ORDER_ID);
        log.info("【订单查询】, {}", orderDTO);
    }

    @Test
    public void findList() {
        PageRequest pageRequest = new PageRequest(0 , 10);
        Page<OrderDTO> page = orderService.findList(BUYER_OPENID, pageRequest);
        Assert.assertEquals(0 , page.getTotalElements());
    }

    @Test
    public void cancelOrder() {
        OrderDTO orderDTO = orderService.findOne(ORDER_ID);
        OrderDTO result = orderService.cancelOrder(orderDTO);
        Assert.assertEquals(OrderStatusEnum.CANCEL.getCode(), result.getOrderStatus());
    }

    @Test
    public void finishOrder() {
        OrderDTO orderDTO = orderService.findOne(ORDER_ID);
        OrderDTO result = orderService.finishOrder(orderDTO);
        Assert.assertEquals(OrderStatusEnum.FINISHED.getCode(), result.getOrderStatus());
    }

    @Test
    public void paidOrder() {
        OrderDTO orderDTO = orderService.findOne(ORDER_ID);
        OrderDTO result = orderService.paidOrder(orderDTO);
        Assert.assertEquals(PayStatusEnum.SUCCESS.getCode(), result.getPayStatus());
    }
}