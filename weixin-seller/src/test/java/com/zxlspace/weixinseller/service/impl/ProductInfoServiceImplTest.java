package com.zxlspace.weixinseller.service.impl;

import com.zxlspace.weixinseller.dataobject.ProductInfo;
import com.zxlspace.weixinseller.enums.ProductStatusEnum;
import com.zxlspace.weixinseller.service.ProductInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductInfoServiceImplTest {
    @Autowired
    private ProductInfoService productInfoService;

    @Test
    public void findOne() {
        ProductInfo productInfo = productInfoService.findOne("fd270b97-e1f8-ass");
        System.out.println(productInfo);
    }

    @Test
    public void findUpAll() {
        List<ProductInfo> productInfoList = productInfoService.findUpAll();
        System.out.println("商品数量：" + productInfoList.size());
    }

    @Test
    public void findAll() {
        PageRequest pageRequest = new PageRequest(0, 10);
        Page<ProductInfo> page = productInfoService.findAll(pageRequest);
        System.out.println(page.getTotalElements());
        System.out.println(page.getTotalPages());
        System.out.println(page.getContent());
    }

    @Test
    public void save() {
        ProductInfo productInfo = new ProductInfo().builder()
                .productId(UUID.randomUUID().toString().substring(0, 16)).productName("Iphone 8 PLUS")
                .productPrice(new BigDecimal("6666.99").setScale(BigDecimal.ROUND_CEILING))
                .productDescription("苹果智能手机")
                .categoryType(2).productIcon("http://static.roncoo.com/lecturer/ec8a20af4dcd42bfb88a25bc34ca5d97.jpg")
                .productStatus(1)
                .productStock(1000)
                .build();

        ProductInfo result = productInfoService.save(productInfo);
        System.out.println(result);
    }
}