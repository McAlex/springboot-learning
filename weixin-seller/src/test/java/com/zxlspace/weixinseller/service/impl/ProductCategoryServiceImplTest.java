package com.zxlspace.weixinseller.service.impl;

import com.zxlspace.weixinseller.dataobject.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryServiceImplTest {
    @Autowired
    private ProductCategoryServiceImpl productCategoryService;

    @Test
    public void findOne() {
        ProductCategory productCategory = productCategoryService.findOne(1);
        System.out.println(productCategory);
        Assert.assertEquals(new Integer(1), productCategory.getCategoryId());
    }

    @Test
    public void findAll() {
        List<ProductCategory> productCategoryList = productCategoryService.findAll();
       for (ProductCategory category : productCategoryList) {
           System.out.println(category);
       }
    }

    @Test
    public void findByCategoryTypeIn() {
        List<Integer> idList = Arrays.asList(1, 2, 3);
        List<ProductCategory> productCategoryList = productCategoryService.findByCategoryTypeIn(idList);
        for (ProductCategory category : productCategoryList) {
            System.out.println(category);
        }
    }

    @Test
    public void save() {
        //新增
        /*ProductCategory productCategory = new ProductCategory("秒杀榜", 5);
        ProductCategory result = productCategoryService.save(productCategory);
        System.out.println(result);*/

        ProductCategory productCategory = productCategoryService.findOne(7);
        productCategory.setCategoryType(6);
        ProductCategory result = productCategoryService.save(productCategory);
        System.out.println(result);
    }
}