package com.zxlspace.weixinseller.repository;

import com.zxlspace.weixinseller.dataobject.ProductCategory;
import com.zxlspace.weixinseller.dataobject.ProductInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductInfoRepositoryTest {
    @Autowired
    private ProductInfoRepository productInfoRepository;

    @Test
    public void save() {
        ProductInfo productInfo = new ProductInfo().builder()
                .productId(UUID.randomUUID().toString().substring(0, 16)).productName("Iphone X")
                .productPrice(new BigDecimal("9999.99").setScale(BigDecimal.ROUND_CEILING))
                .productDescription("苹果智能手机")
                .categoryType(2).productIcon("http://static.roncoo.com/lecturer/ec8a20af4dcd42bfb88a25bc34ca5d97.jpg")
                .productStatus(0)
                .productStock(1000)
                .build();

      ProductInfo result = productInfoRepository.save(productInfo);
        System.out.println(result);

    }

    @Test
    public void findByProductStatus() {
        List<ProductInfo> productInfos = productInfoRepository.findByProductStatus(0);
        Assert.assertEquals(1, productInfos.size());
    }

    @Test
    public void findByCategoryType() {
        List<Integer> idList = Arrays.asList(1, 2, 3);
        List<ProductInfo> productInfoList = productInfoRepository.findByCategoryType(2);
        for (ProductInfo productInfo : productInfoList) {
            System.out.println(productInfo);
        }
    }
}