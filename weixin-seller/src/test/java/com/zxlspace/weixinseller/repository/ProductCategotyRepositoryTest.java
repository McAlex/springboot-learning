package com.zxlspace.weixinseller.repository;

import com.zxlspace.weixinseller.dataobject.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategotyRepositoryTest {
    @Autowired
    private ProductCategotyRepository categotyRepository;

    @Test
    public void test() {
        ProductCategory productCategory = categotyRepository.findOne(1);
        System.out.println(productCategory.toString());
    }

    @Test
    public void testAdd() {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setCategoryName("女生最爱").setCategoryType(3);
        categotyRepository.save(productCategory);
    }

    @Test
    public void testUpdate() {
       /* ProductCategory productCategory = new ProductCategory();
        productCategory.setCategoryId(2).setCategoryName("男生最爱").setCategoryType(3);*/
        ProductCategory productCategory = categotyRepository.findOne(2);
        productCategory.setCategoryType(10);
        categotyRepository.save(productCategory);
    }

    @Test
    @Transactional  //@Transactional 注解和通常spring中的事务注解不同，此注解在测试中的意义是测试产生数据会回滚不会污染数据库
    public void saveTest() {
        ProductCategory productCategory = new ProductCategory("男生最爱", 5);
        categotyRepository.save(productCategory);
    }

    @Test
    public void getCategoryList() {
        List<Integer> list = Arrays.asList(2, 3, 4);
        List<ProductCategory> productCategoryList = categotyRepository.findByCategoryTypeIn(list);
        int result = productCategoryList.size();
        System.out.println(productCategoryList);
        Assert.assertNotEquals(0, result);
    }
}