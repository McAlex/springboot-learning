package com.zxlspace.weixinseller.dataobject;

import com.zxlspace.weixinseller.enums.PayStatusEnum;
import com.zxlspace.weixinseller.enums.OrderStatusEnum;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单信息
 * @Package com.zxlspace.weixinseller.dataobject
 * @Description:
 * @date 2018/6/14 15:50
 */
@Data
@Entity
@DynamicUpdate
public class OrderMaster {
    //订单id
    @Id
    private String orderId;
    //买家姓名
    private String buyerName;
    //买家电话
    private String buyerPhone;
    //买家地址
    private String buyerAddress;
    //买家微信openid
    private String buyerOpenId;
    //订单总金额
    private BigDecimal orderAmount;
    //订单状态
    private Integer orderStatus = OrderStatusEnum.NEW.getCode();
    //支付状态
    private Integer payStatus = PayStatusEnum.WAIT.getCode();

    private Date createTime;

    private Date updateTime;
}
