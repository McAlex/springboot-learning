package com.zxlspace.weixinseller.dataobject;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 类目
 * @Package com.zxlspace.weixinseller.dataobject
 * @Description:
 * @date 2018/6/14 11:21
 */
@Data
@Accessors(chain = true)
@Entity //自动映射实体
@DynamicUpdate //自动更新update时间
public class ProductCategory extends BaseDto{
    //类目id
    @Id
    @GeneratedValue
    private Integer categoryId;
    //类目名字
    private String categoryName;
    //类目编号
    private Integer categoryType;

    public ProductCategory() {
    }

    public ProductCategory(String categoryName, Integer categoryType) {
        this.categoryName = categoryName;
        this.categoryType = categoryType;
    }
}
