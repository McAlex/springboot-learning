package com.zxlspace.weixinseller.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单创建入参
 * @Package com.zxlspace.weixinseller.form
 * @Description:
 * @date 2018/6/21 17:19
 */
@Data
public class OrderForm {
    @NotEmpty(message = "姓名必填")
    private String name;

    @NotEmpty(message = "手机号必填")
    private String phone;

    @NotEmpty(message = "地址必填")
    private String address;

    @NotEmpty(message = "微信openid必填")
    private String openId;

    @NotEmpty(message = "购物车必填")
    private String items;
}
