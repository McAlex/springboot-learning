package com.zxlspace.weixinseller.exception;

import com.zxlspace.weixinseller.enums.ResultEnum;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 自定义异常
 * @Package com.zxlspace.weixinseller.exception
 * @Description:
 * @date 2018/6/15 16:11
 */
public class SellException extends RuntimeException{
    private Integer code;

    public SellException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }

    public SellException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
