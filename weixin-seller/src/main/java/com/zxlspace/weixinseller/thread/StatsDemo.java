package com.zxlspace.weixinseller.thread;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zhangxiaolin
 * @version V1.0
 * @Title: CountDownLatch 方式运行线程池
 * @Package
 * @Description: TODO
 * @date
 */
public class StatsDemo {
    final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    final static String stateTime = sdf.format(new Date());
}
