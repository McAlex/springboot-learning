package com.zxlspace.weixinseller.enums;

import lombok.Getter;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单状态
 * @Package com.zxlspace.weixinseller.enums
 * @Description:
 * @date 2018/6/15 14:42
 */
@Getter
public enum OrderStatusEnum {
    NEW(0, "新订单"),
    FINISHED(1, "完结"),
    CANCEL(2, "取消订单");

    private Integer code;

    private String msg;

    OrderStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
