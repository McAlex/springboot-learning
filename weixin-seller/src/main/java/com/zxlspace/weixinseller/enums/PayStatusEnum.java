package com.zxlspace.weixinseller.enums;

import lombok.Getter;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单状态
 * @Package com.zxlspace.weixinseller.enums
 * @Description:
 * @date 2018/6/15 14:42
 */
@Getter
public enum PayStatusEnum {
    WAIT(0, "等待支付"),
    SUCCESS(1, "已支付");

    private Integer code;

    private String msg;

    PayStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
