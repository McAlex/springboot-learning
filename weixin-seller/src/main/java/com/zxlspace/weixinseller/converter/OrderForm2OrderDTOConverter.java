package com.zxlspace.weixinseller.converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zxlspace.weixinseller.DTO.OrderDTO;
import com.zxlspace.weixinseller.dataobject.OrderDetail;
import com.zxlspace.weixinseller.enums.ResultEnum;
import com.zxlspace.weixinseller.exception.SellException;
import com.zxlspace.weixinseller.form.OrderForm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title:
 * @Package com.zxlspace.weixinseller.converter
 * @Description:
 * @date 2018/6/21 17:43
 */
public class OrderForm2OrderDTOConverter {

    public static OrderDTO convert(OrderForm orderForm) {
        Gson gson = new Gson();

        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setBuyerName(orderForm.getName());
        orderDTO.setBuyerPhone(orderForm.getPhone());
        orderDTO.setBuyerAddress(orderForm.getAddress());
        orderDTO.setBuyerOpenId(orderForm.getOpenId());

        List<OrderDetail> orderDetailList = new ArrayList<>();
        try {
            orderDetailList = gson.fromJson(orderForm.getItems(), new TypeToken<List<OrderDetail>>(){}.getType());
        }catch (Exception e) {
            throw new SellException(ResultEnum.PARAM_ERROR);
        }
        orderDTO.setOrderDetailList(orderDetailList);
        return orderDTO;
    }
}
