package com.zxlspace.weixinseller.converter;

import com.zxlspace.weixinseller.DTO.OrderDTO;
import com.zxlspace.weixinseller.dataobject.OrderMaster;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: ordermaster转换orderDto
 * @Package com.zxlspace.weixinseller.converter
 * @Description:
 * @date 2018/6/21 11:50
 */
public class OrderMaster2OrderDTOConverter {

    public static OrderDTO convert(OrderMaster orderMaster) {
        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(orderMaster, orderDTO);
        return orderDTO;
    }

    public static List<OrderDTO> convert(List<OrderMaster> orderMasterList) {
        return orderMasterList.stream().map(orderMaster ->
            convert(orderMaster)
        ).collect(Collectors.toList());
    }
}
