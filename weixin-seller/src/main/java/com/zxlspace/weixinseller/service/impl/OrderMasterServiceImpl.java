package com.zxlspace.weixinseller.service.impl;

import com.zxlspace.weixinseller.dataobject.OrderMaster;
import com.zxlspace.weixinseller.repository.OrderMasterRepository;
import com.zxlspace.weixinseller.service.OrderMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单业务实现类
 * @Package com.zxlspace.weixinseller.service.impl
 * @Description:
 * @date 2018/6/14 16:06
 */
@Service
public class OrderMasterServiceImpl implements OrderMasterService {
    @Autowired
    private OrderMasterRepository orderMasterRepository;

    @Override
    public OrderMaster findOne(String orderId) {
        return orderMasterRepository.findOne(orderId);
    }

    @Override
    public List<OrderMaster> findAll() {
        return orderMasterRepository.findAll();
    }

    @Override
    public Page<OrderMaster> findByBuyerName(String buyerName, Pageable pageable) {
        return orderMasterRepository.findByBuyerName(buyerName, pageable);
    }

    @Override
    public Page<OrderMaster> findByBuyerPhone(String buyerPhone, Pageable pageable) {
        return findByBuyerPhone(buyerPhone, pageable);
    }

    @Override
    public Page<OrderMaster> findByBuyerOpenId(String buyerOpenId, Pageable pageable) {
        return findByBuyerOpenId(buyerOpenId, pageable);
    }
}
