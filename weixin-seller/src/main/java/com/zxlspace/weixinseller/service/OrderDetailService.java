package com.zxlspace.weixinseller.service;

import com.zxlspace.weixinseller.dataobject.OrderDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 商品详情业务接口
 * @Package com.zxlspace.weixinseller.service
 * @Description:
 * @date 2018/6/15 14:28
 */
public interface OrderDetailService {

    OrderDetail findOne(String detailId);

    /**
     * 根据订单id查询详情
     * @param orderId
     * @return
     */
    List<OrderDetail> findByOrderId(String orderId);

    List<OrderDetail> findAll();

    /**
     * 分页查询订单详情列表
     * @param pageable
     * @return
     */
    Page<OrderDetail> findAll(Pageable pageable);

    OrderDetail save(OrderDetail orderDetail);
}
