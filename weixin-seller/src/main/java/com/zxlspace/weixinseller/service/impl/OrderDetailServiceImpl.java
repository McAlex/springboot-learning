package com.zxlspace.weixinseller.service.impl;

import com.zxlspace.weixinseller.dataobject.OrderDetail;
import com.zxlspace.weixinseller.repository.OrderDetailRepository;
import com.zxlspace.weixinseller.service.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单详情业务实现
 * @Package com.zxlspace.weixinseller.service.impl
 * @Description:
 * @date 2018/6/15 14:30
 */
@Service
public class OrderDetailServiceImpl implements OrderDetailService {
    @Autowired
    private OrderDetailRepository orderDeatilRepsiotry;

    @Override
    public OrderDetail findOne(String detailId) {
        return orderDeatilRepsiotry.findOne(detailId);
    }

    @Override
    public List<OrderDetail> findByOrderId(String orderId) {
        return orderDeatilRepsiotry.findByOrderId(orderId);
    }

    @Override
    public List<OrderDetail> findAll() {
        return orderDeatilRepsiotry.findAll();
    }

    @Override
    public Page<OrderDetail> findAll(Pageable pageable) {
        return orderDeatilRepsiotry.findAll(pageable);
    }

    @Override
    public OrderDetail save(OrderDetail orderDetail) {
        return orderDeatilRepsiotry.save(orderDetail);
    }

}
