package com.zxlspace.weixinseller.service;

import com.zxlspace.weixinseller.DTO.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单业务接口
 * @Package com.zxlspace.weixinseller.service
 * @Description:
 * @date 2018/6/15 15:53
 */
public interface OrderService {
    /**
     * 创建订单
     * @param orderDTO
     * @return
     */
    OrderDTO create(OrderDTO orderDTO);

    /**
     * 查询单个订单
     * @param orderId
     * @return
     */
    OrderDTO findOne(String orderId);

    /**
     * 查询订单列表
     * @param buyerOpendId
     * @param pageable
     * @return
     */
    Page<OrderDTO> findList(String buyerOpendId, Pageable pageable);

    /**
     * 取消订单
     * @param orderDTO
     * @return
     */
    OrderDTO cancelOrder(OrderDTO orderDTO);

    /**
     * 完结订单
     * @param orderDTO
     * @return
     */
    OrderDTO finishOrder(OrderDTO orderDTO);

    /**
     * 支付订单
     * @param orderDTO
     * @return
     */
    OrderDTO paidOrder(OrderDTO orderDTO);
}
