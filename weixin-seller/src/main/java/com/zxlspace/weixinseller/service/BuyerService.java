package com.zxlspace.weixinseller.service;

import com.zxlspace.weixinseller.DTO.OrderDTO;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 买家订单业务操作
 * @Package com.zxlspace.weixinseller.service
 * @Description:
 * @date 2018/6/22 15:51
 */
public interface BuyerService {
    OrderDTO findOrderOne(String openid, String orderId);

    OrderDTO cancelOrder(String openid, String orderId);
}
