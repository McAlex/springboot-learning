package com.zxlspace.weixinseller.service.impl;

import com.zxlspace.weixinseller.dataobject.ProductCategory;
import com.zxlspace.weixinseller.repository.ProductCategotyRepository;
import com.zxlspace.weixinseller.service.ProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 商品类目业务实现
 * @Package com.zxlspace.weixinseller.service.impl
 * @Description:
 * @date 2018/6/14 14:54
 */
@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {
    @Autowired
    private ProductCategotyRepository categotyRepository;

    @Override
    public ProductCategory findOne(Integer categoryId) {
        return categotyRepository.findOne(categoryId);
    }

    @Override
    public List<ProductCategory> findAll() {
        return categotyRepository.findAll();
    }

    @Override
    public List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList) {
        return categotyRepository.findByCategoryTypeIn(categoryTypeList);
    }

    @Override
    public ProductCategory save(ProductCategory productCategory) {
        return categotyRepository.save(productCategory);
    }
}
