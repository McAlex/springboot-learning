package com.zxlspace.weixinseller.service;

import com.zxlspace.weixinseller.dataobject.OrderMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单业务接口
 * @Package com.zxlspace.weixinseller.service
 * @Description:
 * @date 2018/6/14 16:03
 */
public interface OrderMasterService  {
    /**
     * 根据订单id查询
     * @param orderId
     * @return
     */
    OrderMaster findOne(String orderId);

    List<OrderMaster> findAll();

    /**
     * 根据买家姓名查询订单list
     * @param buyerName
     * @return
     */
    Page<OrderMaster> findByBuyerName(String buyerName, Pageable pageable);

    /**
     * 根据买家电话查询订单list
     * @param buyerPhone
     * @return
     */
    Page<OrderMaster> findByBuyerPhone(String buyerPhone, Pageable pageable);

    /**
     * 根据买家微信openid查询
     * @param buyerOpenId
     * @return
     */
    Page<OrderMaster> findByBuyerOpenId(String buyerOpenId, Pageable pageable);

}
