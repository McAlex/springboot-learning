package com.zxlspace.weixinseller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeixinSellerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeixinSellerApplication.class, args);
	}
}
