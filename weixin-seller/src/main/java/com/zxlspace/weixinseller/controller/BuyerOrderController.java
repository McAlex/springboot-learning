package com.zxlspace.weixinseller.controller;

import com.zxlspace.weixinseller.DTO.OrderDTO;
import com.zxlspace.weixinseller.VO.ResultVO;
import com.zxlspace.weixinseller.converter.OrderForm2OrderDTOConverter;
import com.zxlspace.weixinseller.enums.ResultEnum;
import com.zxlspace.weixinseller.exception.SellException;
import com.zxlspace.weixinseller.form.OrderForm;
import com.zxlspace.weixinseller.service.BuyerService;
import com.zxlspace.weixinseller.service.impl.BuyerServiceImpl;
import com.zxlspace.weixinseller.service.impl.OrderServiceImpl;
import com.zxlspace.weixinseller.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 买家订单controller
 * @Package com.zxlspace.weixinseller.controller
 * @Description:
 * @date 2018/6/21 17:25
 */
@RestController
@RequestMapping(value = "/buyer/order")
@Slf4j
public class BuyerOrderController {
    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private BuyerServiceImpl buyerService;

    //创建订单
    @PostMapping(value = "/create")
    public ResultVO<Map<String, String>> create(@Valid OrderForm orderForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            log.error("【创建订单】参数不正确, orderForm={}", orderForm);
            throw new SellException(ResultEnum.PARAM_ERROR.getCode(), bindingResult.getFieldError().getDefaultMessage());
        }

        OrderDTO orderDTO = OrderForm2OrderDTOConverter.convert(orderForm);
        if (CollectionUtils.isEmpty(orderDTO.getOrderDetailList())) {
            log.error("【创建订单】购物车不能为空");
            throw new SellException(ResultEnum.CART_EMPTY);
        }

        OrderDTO createResult = orderService.create(orderDTO);
        Map<String, String> map = new HashMap<>();
        map.put("orderId", createResult.getOrderId());

        return ResultVOUtil.success(map);
     }

    //订单列表
    @GetMapping(value = "/list")
    public ResultVO<List<OrderDTO>> queryOrderList(@RequestParam("openid") String openid, @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                   @RequestParam(value = "size", defaultValue = "10") Integer size) {
        if (StringUtils.isEmpty(openid)) {
            log.error("【查询订单列表】openid不能为空");
            throw new SellException(ResultEnum.PARAM_ERROR);
        }

        PageRequest pageRequest = new PageRequest(page, size);
        Page<OrderDTO>  orderDTOPage = orderService.findList(openid, pageRequest);
        if (!CollectionUtils.isEmpty(orderDTOPage.getContent())) {
            return ResultVOUtil.success(orderDTOPage.getContent());
        }
        return ResultVOUtil.success(null);
    }

    //订单详情
    @GetMapping("/detail")
    public ResultVO<OrderDTO> detail(@RequestParam("openid")String openid, @RequestParam("orderId") String orderId) {
        if (StringUtils.isEmpty(orderId)) {
            log.error("【查询订单详情】orderId不能为空");
            throw new SellException(ResultEnum.PARAM_ERROR);
        }

        OrderDTO orderDTO = buyerService.findOrderOne(openid, orderId);
        return ResultVOUtil.success(orderDTO);
    }

    //取消订单
    @PostMapping("/cancel")
    public ResultVO cancel(@RequestParam("openid")String openid, @RequestParam("orderId") String orderId) {
        if (StringUtils.isEmpty(orderId)) {
            log.error("【取消订单】orderId不能为空");
            throw new SellException(ResultEnum.PARAM_ERROR);
        }

        buyerService.cancelOrder(openid, orderId);
        return ResultVOUtil.success();
    }

}
