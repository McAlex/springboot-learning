package com.zxlspace.weixinseller.controller;

import com.zxlspace.weixinseller.VO.ProductInfoVO;
import com.zxlspace.weixinseller.VO.ProductVO;
import com.zxlspace.weixinseller.VO.ResultVO;
import com.zxlspace.weixinseller.dataobject.ProductCategory;
import com.zxlspace.weixinseller.dataobject.ProductInfo;
import com.zxlspace.weixinseller.service.impl.ProductCategoryServiceImpl;
import com.zxlspace.weixinseller.service.impl.ProductInfoServiceImpl;
import com.zxlspace.weixinseller.utils.ResultVOUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 买家商品controller
 * @Package com.zxlspace.weixinseller.controller
 * @Description:
 * @date 2018/6/14 16:46
 */
@RestController
@RequestMapping("/buyer/product")
public class BuyerProductController {
    @Autowired
    private ProductCategoryServiceImpl productCategoryService;

    @Autowired
    private ProductInfoServiceImpl productInfoService;

    @RequestMapping("/list")
    public ResultVO list() {
        //查询所有上架商品
        List<ProductInfo> productInfoList = productInfoService.findUpAll();
        //获取所有商品的
        List<Integer> categoryTypeList = new ArrayList<>();
        HashSet<Integer> categoryTypeSet = new HashSet<>();
        for (ProductInfo productInfo : productInfoList) {
            //categoryTypeList.add(productInfo.getCategoryType());
            categoryTypeSet.add(productInfo.getCategoryType());
        }

        for(Integer type : categoryTypeSet) {
            categoryTypeList.add(type);
        }
        //查询所有商品类目
        List<ProductCategory> productCategoryList = productCategoryService.findByCategoryTypeIn(categoryTypeList);

        //首页商品列表vo
        List<ProductVO> productVOList = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productCategoryList) && !CollectionUtils.isEmpty(productInfoList)) {
            for (ProductCategory productCategory : productCategoryList) {
                ProductVO productVO = new ProductVO();
                productVO.setCategoryName(productCategory.getCategoryName());
                productVO.setCategoryType(productCategory.getCategoryType());
                //商品详情列表vo
                List<ProductInfoVO> productInfoVOList = new ArrayList<>();

                for (ProductInfo productInfo : productInfoList) {
                    if (productCategory.getCategoryType().equals(productInfo.getCategoryType())) {
                        ProductInfoVO productInfoVO = new ProductInfoVO();
                        BeanUtils.copyProperties(productInfo, productInfoVO);
                        productInfoVOList.add(productInfoVO);
                    }
                }
                productVO.setProductInfoVOList(productInfoVOList);
                productVOList.add(productVO);
            }
        }

        return ResultVOUtil.success(productVOList);
    }
}
