package com.zxlspace.weixinseller.utils;

import com.zxlspace.weixinseller.VO.ResultVO;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: vo返回对象工具类
 * @Package com.zxlspace.weixinseller.utils
 * @Description:
 * @date 2018/6/15 11:44
 */
public class ResultVOUtil {
    public static ResultVO success(Object object) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(0).setMsg("操作成功").setData(object);
        return resultVO;
    }

    public static ResultVO success() {
        return success(null);
    }

    public static ResultVO error(Integer code, String msg) {
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(code).setMsg(msg).setData(null);
        return resultVO;
    }

}
