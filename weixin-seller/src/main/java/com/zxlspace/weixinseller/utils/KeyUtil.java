package com.zxlspace.weixinseller.utils;

import java.util.Random;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 随机id生产工具类
 * @Package com.zxlspace.weixinseller.utils
 * @Description:
 * @date 2018/6/20 15:57
 */
public class KeyUtil {

    /**
     * 生产唯一的主键
     * 格式：时间戳 + 随机数
     * @return
     */
    public static synchronized String genUniqueKey() {
        Random random = new Random();
        Integer number = random.nextInt(900000) + 100000;
        return System.currentTimeMillis() + String.valueOf(number);
    }
}
