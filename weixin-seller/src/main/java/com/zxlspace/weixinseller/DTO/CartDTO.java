package com.zxlspace.weixinseller.DTO;

import lombok.Data;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 购物车对象
 * @Package com.zxlspace.weixinseller.DTO
 * @Description:
 * @date 2018/6/20 16:25
 */
@Data
public class CartDTO {
    /**商品id**/
    private String productId;
    /**商品数量**/
    private Integer productQuantity;

    public CartDTO(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
