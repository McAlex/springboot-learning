package com.zxlspace.weixinseller.DTO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zxlspace.weixinseller.dataobject.OrderDetail;
import com.zxlspace.weixinseller.utils.serializer.Date2LongSerializer;
import lombok.Data;

import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单转换对象
 * @Package com.zxlspace.weixinseller.DTO
 * @Description:
 * @date 2018/6/15 15:48
 */
@Data
public class OrderDTO {
    //订单id
    @Id
    private String orderId;
    //买家姓名
    private String buyerName;
    //买家电话
    private String buyerPhone;
    //买家地址
    private String buyerAddress;
    //买家微信openid
    private String buyerOpenId;
    //订单总金额
    private BigDecimal orderAmount;
    //订单状态
    private Integer orderStatus;
    //支付状态
    private Integer payStatus;

    //创建时间
    @JsonSerialize(using = Date2LongSerializer.class)
    private Date createTime;

    //更新时间
    @JsonSerialize(using = Date2LongSerializer.class)
    private Date updateTime;

    private List<OrderDetail> orderDetailList;
}
