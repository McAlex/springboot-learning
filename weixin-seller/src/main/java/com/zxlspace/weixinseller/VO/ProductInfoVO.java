package com.zxlspace.weixinseller.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 商品详情
 * @Package com.zxlspace.weixinseller.VO
 * @Description:
 * @date 2018/6/14 17:36
 */
@Data
public class ProductInfoVO {
    @JsonProperty("id")
    private String productId;

    @JsonProperty("name")
    private String productName;

    @JsonProperty("price")
    private BigDecimal productPrice;

    @JsonProperty("description")
    private String productDescription;

    @JsonProperty("icon")
    private String productIcon;
}
