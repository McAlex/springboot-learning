package com.zxlspace.weixinseller.VO;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: http 视图最外层返回对象
 * @Package com.zxlspace.weixinseller.VO
 * @Description:
 * @date 2018/6/14 16:54
 */
@Data
@Accessors(chain = true)
public class ResultVO<T> {
    //错误码
    private Integer code;
    //错误信息
    private String msg;
    //返回结果集
    private T data;
}
