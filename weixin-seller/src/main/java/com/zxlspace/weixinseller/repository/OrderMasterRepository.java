package com.zxlspace.weixinseller.repository;

import com.zxlspace.weixinseller.dataobject.OrderMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 订单资源库
 * @Package com.zxlspace.weixinseller.repository
 * @Description:
 * @date 2018/6/14 15:55
 */
public interface OrderMasterRepository extends JpaRepository<OrderMaster, String> {

    Page<OrderMaster> findByBuyerName(String buyerName, Pageable pageable);

    Page<OrderMaster> findByBuyerPhone(String buyerPhone, Pageable pageable);

    Page<OrderMaster> findByBuyerOpenId(String buyerOpenId, Pageable pageable);
}
