package com.zxlspace.weixinseller.repository;

import com.zxlspace.weixinseller.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zhangxiaolin@zhonhajt.com
 * @Title: 商品详情资源库
 * @Package com.zxlspace.weixinseller.repository
 * @Description:
 * @date 2018/6/15 14:26
 */
public interface OrderDetailRepository extends JpaRepository<OrderDetail, String> {

    /**
     * 通过订单id查询订单详情，一个订单可能对应多个订单详情
     * @param orderId
     * @return
     */
    List<OrderDetail> findByOrderId(String orderId);
}
