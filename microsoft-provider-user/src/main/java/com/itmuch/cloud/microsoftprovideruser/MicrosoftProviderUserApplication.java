package com.itmuch.cloud.microsoftprovideruser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrosoftProviderUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrosoftProviderUserApplication.class, args);
	}

}
